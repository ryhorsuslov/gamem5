﻿using System;
using System.Drawing;

namespace GameM5
{
    class Program
    {
        static Point start = new Point { X = 1, Y = 1 };
        static Random rnd = new Random();
        static bool gameover;
        static void Main()
        {
            Console.CursorVisible = false;
            DrawBorderAndGoal();
            Drawhero(start);
            Game();
            if (gameover == true)
            {
                Console.WriteLine("\n" + "ещё разок? Y/N");
                string inpt = Console.ReadLine();
                if (inpt == "y" || inpt == "Y")
                {
                    Console.Clear();
                    Main();
                }
            }
        }
        static void Game()
        {
            int helth = 10;
            Point curretp = start;
            Point end = new Point { X = 10, Y = 10 };
            int count = 10;
            Point[] a = ItsaTrap(count);
            ConsoleKey key = Console.ReadKey().Key;
            Console.Clear();
            while (helth > 0)
            {
                if (curretp.X == end.Y && curretp.Y == end.Y)
                    Console.SetCursorPosition(curretp.X, curretp.Y);
                switch (key)
                {
                    case ConsoleKey.UpArrow:
                        {
                            if (curretp.Y > 1)
                            {
                                curretp.Y--;

                            }
                            Drawhero(curretp);
                            break;
                        }
                    case ConsoleKey.DownArrow:
                        {
                            if (curretp.Y < 10)
                            {
                                curretp.Y++;
                            }
                            Drawhero(curretp);
                            break;
                        }
                    case ConsoleKey.LeftArrow:
                        {
                            if (curretp.X > 1)
                            {
                                curretp.X--;

                            }
                            Drawhero(curretp);
                            break;
                        }
                    case ConsoleKey.RightArrow:
                        {
                            if (curretp.X < 10)
                            {
                                curretp.X++;

                            }
                            Drawhero(curretp);
                            break;
                        }
                }
                for (int i = 0; i < count; i++)
                {
                    int dmgcontr;
                    bool b = a[i].Equals(curretp);
                    if (b == true)
                    {
                        dmgcontr = rnd.Next(1, 10);
                        helth -= dmgcontr;
                    }
                }
                DrawBorderAndGoal();
                Console.WriteLine("\n" + "helth = " + helth);
                //for (int i = 0; i < count; i++)
                //    Console.Write(a[i]);

                if (curretp.Equals(end) == true)
                {
                    Console.Write("\n u win");
                    break;
                }
                key = Console.ReadKey().Key;
                Console.Clear();
            }
            gameover = true;
        }
        static void DrawBorderAndGoal()
        {
            Console.SetCursorPosition(10, 10);
            Console.Write("G");
            for (int i = 0; i <= 11; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write("#");
                Console.SetCursorPosition(11, i);
                Console.Write("#");
            }
            Console.SetCursorPosition(0, 10);
            for (int i = 0; i <= 11; i++)
            {
                Console.SetCursorPosition(i, 0);
                Console.Write("#");
                Console.SetCursorPosition(i, 11);
                Console.Write("#");
            }
            
        }
        static void Drawhero(Point p)
        {
            Console.SetCursorPosition(p.X, p.Y);
            Console.Write("H");
        }
        static Point[] ItsaTrap(int count)
        {
            Point[] arr = new Point[count];
            for (int i = 0; i < count; i++)
            {
                int x = rnd.Next(1, 10);
                int y = rnd.Next(1, 10);
                arr[i] = new Point { X = x, Y = y };
                if ((arr[i].X ==1 && arr[i].Y==1 ) || (arr[i].X == 10 && arr[i].Y == 10))
                {
                    arr[i].X = rnd.Next(1, 10);
                    arr[i].Y = rnd.Next(1, 10);
                }
            }
            return arr;
        }
    }
}



